// import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Component from './Latihan/Component/Component';
import YoutubeUI from './Tugas/Tugas12/App'
import MockUp from './Tugas/Tugas13/App'
import Note from './Tugas/Tugas14/App'
import Navi from './Tugas/Tugas15/index'
import NaviSkill from './Tugas/Tugas15//TugasNavigation/index'
import Quiz3 from './Tugas/quiz3/index'

export default function App() {
// const App = () => {
  return (
    <Quiz3 />
    // <Component />
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
// export default App;
