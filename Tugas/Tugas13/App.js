import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity
} from 'react-native'
import Login from './LoginScreen';
import Register from './RegisterScreen';
import Skills from './SkillsScreen';
import About from './AboutScreen';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Skills />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})
