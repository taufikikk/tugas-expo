import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import sanbercode from './images/sanbercode.png';
export default class Register extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={sanbercode} />
        </View>
        <View style={styles.textInputWrapper}>
          <Text>Username</Text>
          <TextInput
            style={styles.textInputField}
          />
        </View>
        <View style={styles.textInputWrapper}>
          <Text>Email</Text>
          <TextInput
            style={styles.textInputField}
          />
        </View>
        <View style={styles.textInputWrapper}>
          <Text>Password</Text>
          <TextInput style={styles.textInputField} />
        </View>
        <View style={styles.textInputWrapper}>
          <Text>Confirm Password</Text>
          <TextInput style={styles.textInputField} />
        </View>
        <View style={styles.textInputWrapper}>
          <Text>Nama Lengkap</Text>
          <TextInput style={styles.textInputField} />
        </View>
        <View style={styles.textInputWrapper}>
          <Text>No Handphone</Text>
          <TextInput style={styles.textInputField} />
        </View>
        <View style={styles.textButtonWrapper}>
          <TouchableOpacity style={styles.buttonTouchable}>
            <Text style={styles.textButton}>Submit</Text>
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    marginVertical: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40,
  },
  textInputWrapper: {
    alignItems: 'center',
    marginBottom: 5,
  },
  textInputField: {
    height: 32,
    width: 290,
    backgroundColor: '#F4FAFF',
  },
  buttonTouchable: {
    marginBottom: 20,
    height: 26,
    width: 95,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#53B7FF',
  },
  textButtonWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButton: {
    color: 'white',
  },
});
