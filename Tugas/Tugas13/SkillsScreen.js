import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import sanbercode from './images/sanbercode.png';
import java from './images/java.png'
import js from './images/js.png'
import laravel from './images/laravel.png'
import mysql from './images/mysql.png'
import php from './images/php.png'
import react from './images/react.png'
import gitlab from './images/gitlab.png'
export default class Skills extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={sanbercode} style={{width: 180, height: 22}}/>
        </View>
        <Text style={styles.halo}>Halo, Taufiq</Text>
        <Text style={styles.skills}>SKILLS</Text>
        <Text>------------------------------------------------</Text>
        <View style={styles.group1}>
        <Text style={styles.titleGroup}>Bahasa Pemrograman</Text>
          <View style={styles.element}>
            <Text>Intermediate Java</Text>
            < View style={styles.rectangle}/>
            < View style={styles.rectangle2}/>
            <Text style={styles.percent}>40%</Text>
            <Image source={java} style={styles.icons}/>
          </View>
          <View style={styles.element}>
            <Text>Intermediate PHP</Text>
            < View style={styles.rectangle}/>
            < View style={styles.rectangle3}/>
            <Text style={styles.percent}>40%</Text>
            <Image source={php} style={styles.icons}/>
            </View>
          <View style={styles.element}>
            <Text>Basic Javascript</Text>
            < View style={styles.rectangle}/>
            < View style={styles.rectangle4}/>
            <Text style={styles.percent}>40%</Text>
            <Image source={js} style={styles.icons}/>
          </View>
        </View>
        <View style={styles.group2}>
        <Text style={styles.titleGroup}>Framework</Text>
          <View style={styles.element}>
            <Text>Intermediate Laravel</Text>
            < View style={styles.rectangle}/>
            < View style={styles.rectangle5}/>
            <Text style={styles.percent}>40%</Text>
            <Image source={laravel} style={styles.icons}/>
            </View>
          <View style={styles.element}>
            <Text>Basic React Native</Text>
            < View style={styles.rectangle}/>
            < View style={styles.rectangle4}/>
            <Text style={styles.percent}>40%</Text>
            <Image source={react} style={styles.icons}/>
          </View>
        </View>
        <View style={styles.group3}>
        <Text style={styles.titleGroup}>Technology</Text>
          <View style={styles.element}>
            <Text>Intermediate MySQL</Text>
            < View style={styles.rectangle}/>
            < View style={styles.rectangle5}/>
            <Text style={styles.percent}>40%</Text>
            <Image source={mysql} style={styles.icons}/>
          </View>
        </View>
          <View style={styles.element}>
          <TouchableOpacity style={styles.buttonTouchable}>
            <Text style={styles.textButton}>About</Text>
          </TouchableOpacity>
          </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    marginVertical: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 25,
  },
  element: {
    marginBottom: 10,
    marginHorizontal: 60,
  },
  rectangle: {
    height: 32,
    width: 290,
    backgroundColor: '#F4FAFF',
  },
  rectangle2: {
    marginTop: -28,
    marginLeft: 15,
    height: 22,
    width: 210,
    backgroundColor: '#F5BF6E',
  },
  rectangle3: {
    marginTop: -28,
    marginLeft: 15,
    height: 22,
    width: 210,
    backgroundColor: '#A4F77D',
  },
  rectangle4: {
    marginTop: -28,
    marginLeft: 15,
    height: 22,
    width: 210,
    backgroundColor: '#81F56E',
  },
  rectangle5: {
    marginTop: -28,
    marginLeft: 15,
    height: 22,
    width: 210,
    backgroundColor: '#C1F56E',
  },
  buttonTouchable: {
    marginTop :25,
    marginLeft: 85,
    height: 26,
    width: 95,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#53B7FF',
  },
  textButtonWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButton: {
    color: 'white',
  },
  titleGroup: {
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 50,
    marginTop: 5
  },
  halo: {
    marginLeft: 50,
    marginBottom: 10,
  },
  skills: {
    marginLeft: 50,
    marginBottom: 10,
    fontSize: 26,
    fontWeight: 'bold',
  },
  percent: {
    marginTop: -20,
    marginLeft: 140,

  },
  icons: {
    marginLeft: 300,
    marginTop: -36,
    width: 40,
    height: 35,
  }
});
