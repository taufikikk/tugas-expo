import React from 'react';
import Main from './components/Main';
import Skills from './SkillScreen'
import data from './skillData.json';

export default class App extends React.Component {
  render() {
    return (
      <Skills />
    );
  }
}