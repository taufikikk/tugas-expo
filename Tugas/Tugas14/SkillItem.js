import React, {Component} from 'react';
import {
  Text, 
  StyleSheet, 
  View, 
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class SkillItem extends Component {
  render() {
    let skill = this.props.items;
    return (
        <View style={styles.container}>

          <View style={styles.skillContent}>
            <Icon style={styles.icons}
            name={skill.iconName}
            color="#003366"
            size={70}
            />
            <View style={styles.skillWrapper}>
              <Text style={styles.textSkill}>{skill.skillName}</Text>
              <Text style={styles.textCategory}>{skill.categoryName}</Text>
              <Text style={styles.textPercent}>{skill.percentageProgress}</Text>
            </View>
            <Icon style={styles.iconRight}
            name="chevron-right"
            color="#003366"
            size={90}
            />
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
  },
  skillContent: {
    width: 343,
    height: 129,
    marginRight:40,
    marginLeft:35,
    marginTop:5,
    backgroundColor: '#B4E9FF',
    flexDirection: 'row',
    borderRadius:8,
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16,
    
  },
  percent: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },

  textSkill: {
    fontSize: 24,
    color: '#003366',
    fontWeight: 'bold',
    lineHeight: 28,
  },
  textPercent: {
    marginLeft:80,
    marginRight: 15,
    fontSize: 48,
    color: '#FFFFFF',
    fontWeight: 'bold',
    lineHeight: 56,
  },
    icons: {
      marginTop: 25,
      marginLeft: 5,
      width: 70,
      height: 70,
  },
  textCategory: {
    fontSize: 16,
    color: '#3EC6FF',
    fontWeight: 'bold',
    lineHeight: 19,
  },
  skillWrapper: {
    marginLeft: 5,
    marginTop: 20,
  },
  iconRight: {
    marginTop: 25,
  }
});
