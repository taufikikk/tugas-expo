import React, {Component} from 'react';
import {
  StyleSheet, 
  View, 
  FlatList
} from 'react-native';

import SkillData from './skillData.json';
import SkillItem from './SkillItem';

export default class Skills extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: SkillData.items,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.item}
          renderItem={skill => <SkillItem items={skill.item} />}
          keyExtractor={item => item.id.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
