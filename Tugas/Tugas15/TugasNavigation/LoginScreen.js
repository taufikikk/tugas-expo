import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import sanbercode from './images/sanbercode.png';

export default class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={sanbercode} />
        </View>
        <View style={styles.textInputWrapper}>
          <Text style={styles.label}>Username/Email</Text>
          <TextInput
            style={styles.textInputField}
          />
        </View>
        <View style={styles.textInputWrapper}>
          <Text style={styles.label}>Password</Text>
          <TextInput style={styles.textInputField} />
        </View>
        <View style={styles.textButtonWrapper}>
          <TouchableOpacity style={styles.buttonTouchable}
            onPress={() => this.props.navigation.push('Home')}>
            <Text style={styles.textButton}>Submit</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    marginVertical: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInputWrapper: {
    alignItems: 'center',
    marginBottom: 5,
  },
  textInputField: {
    height: 32,
    width: 207,
    backgroundColor: '#F4FAFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'black',
  },
  buttonTouchable: {
    marginBottom: 70,
    height: 26,
    width: 95,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#53B7FF',
  },
  textButtonWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    color: 'black',
    textAlign: 'center',

  },
  textButton: {
    color: 'white',
  },
});
