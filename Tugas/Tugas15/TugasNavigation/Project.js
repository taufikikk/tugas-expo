import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default function Project() {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Halaman Project</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  text: {
    fontSize: 15,
  },
});
