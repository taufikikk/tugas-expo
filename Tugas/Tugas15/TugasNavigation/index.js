import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import Login from './LoginScreen';
import About from './AboutScreen';
import Skills from './SkillsScreen';
import Add from './Add';
import Project from './Project';

const RootStack = createStackNavigator();
const BottomTabStack = createBottomTabNavigator();
const DrawerStack = createDrawerNavigator();

function Root() {
  return (
    <RootStack.Navigator>
      <RootStack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <RootStack.Screen
        name="Home"
        component={Drawer}
        options={{title: 'Root'}}
      />
    </RootStack.Navigator>
  );
}

function BottomTab() {
  return (
    <BottomTabStack.Navigator>
      <BottomTabStack.Screen name="Skills" component={Skills} />
      <BottomTabtack.Screen name="Project" component={Project} />
      <BottomTabStack.Screen name="Add" component={Add} />
    </BottomTabStack.Navigator>
  );
}


function Drawer() {
    return (
      <DrawerStack.Navigator>
        <DrawerStack.Screen name="About" component={About} />
        <DrawerStack.Screen name="Tabs" component={BottomTab} />
      </DrawerStack.Navigator>
    );
  }

  export default () => (
    <NavigationContainer>
      <Root />
    </NavigationContainer>
)

const styles = StyleSheet.create({

});
